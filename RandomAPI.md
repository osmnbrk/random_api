﻿# Rastgele Mesafe API Dokümantasyonu
	API 8787 portundan çalışmaktadır. Çalıştırabilmek için flask ve flask_restful kütüphanelerine gerek vardır.
## Endpointler
### {GET} --- /distance
 #### Parametreler
	 Parametreler body içinde x-www-form-urlencoded veya query param şeklinde gönderilebilir.
 1. type --> Hedef noktasının warehouse("w") veya line("l") olduğunu belirtir.
 2. id --> Tipi belirtilen hedefin id'si.
 #### Örnek request linki
	 http://localhost:8787/distance?id=4&type=w
