from flask_restful import reqparse, abort, Resource
import random

parser = reqparse.RequestParser()

parser.add_argument('warehouse_id', type = str)


class Distance(Resource):
    def get(self):
        args = parser.parse_args()
        temp_list = []
        temp_dict = {}
        if int(args.warehouse_id) > 12:
            temp_dict["Name"] = "1"
            temp_dict["Distance"] = str(random.randrange(0,100))
        else:
            temp_dict["Name"] = "2"
            temp_dict["Distance"] = str(random.randrange(0,100))
        temp_list.append(temp_dict)
        return temp_list
        
    def post(self):
        pass
    def put(self):
        pass
    def delete(self):
        pass