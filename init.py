from flask import Flask
from flask_restful import reqparse, abort, Api, Resource
from endpoints import Distance
app = Flask(__name__)
api = Api(app)

api.add_resource(Distance.Distance, '/distance')


if __name__ == '__main__':
    app.run(debug = False , host="0.0.0.0", port=8787)